package pe.com.claro.shell.crearbolsaaltaporta.service;

import pe.com.claro.shell.crearbolsaaltaporta.exception.DBException;

public interface MainService {
	
	public void run(String idTransaccion) throws DBException;
}
