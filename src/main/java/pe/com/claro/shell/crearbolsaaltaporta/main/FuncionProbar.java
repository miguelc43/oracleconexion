package pe.com.claro.shell.crearbolsaaltaporta.main;

import java.io.Serializable;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Types;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.ws.rs.core.Configuration;
import javax.ws.rs.core.Context;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import pe.com.claro.shell.crearbolsaaltaporta.util.Constantes;

public class FuncionProbar implements Serializable {

	@Context
	private static Configuration configuration;
	
	private static final Logger LOG = LoggerFactory.getLogger(FuncionProbar.class);
	private static float numero;
	public static void main(String[] args) throws ClassNotFoundException {
		// TODO Auto-generated method stub
		//StringBuffer storedProcedure= new StringBuffer();
		//storedProcedure.append(configuration.getProperty("db.bscs.owner").toString());
		//storedProcedure.append(Constantes.PUNTO);
		//storedProcedure.append(configuration.getProperty("db.bscs.pkg.siac_migracion_clfy").toString());
		//storedProcedure.append(Constantes.PUNTO);
		//storedProcedure.append(configuration.getProperty("db.cobs.sp.show_list_element").toString());
		
		try {
			Class.forName("oracle.jdbc.OracleDriver");
			CallableStatement sentencia;
			Connection conexion = DriverManager.getConnection("jdbc:oracle:thin:@172.19.32.101:1521:BSCSDES", "tim", "tim");
			sentencia = conexion.prepareCall("{?=call TFUN006_GET_COID_FROM_DN(?)}");
			sentencia.registerOutParameter(1, Types.VARCHAR);
			sentencia.setString(2,"980594679"); 
			sentencia.executeQuery();     
			numero=sentencia.getFloat(1);
			System.out.println(numero);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}//Declarar la sentencia.
		
        
	}

}
