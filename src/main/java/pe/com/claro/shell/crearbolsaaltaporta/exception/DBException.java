package pe.com.claro.shell.crearbolsaaltaporta.exception;

public class DBException extends BaseException {

	private static final long serialVersionUID = 3812762141784699905L;

	public DBException() {
		super();
	}

	public DBException(String msjError, Exception objexception) {
		super(msjError, objexception);
		// TODO Auto-generated constructor stub
	}

	public DBException(String msjError) {
		super(msjError);
		// TODO Auto-generated constructor stub
	}

	public DBException(String codError, String msjError, Exception objexception) {
		super(codError, msjError, objexception);
		// TODO Auto-generated constructor stub
	}

	public DBException(String codError, String msjError, String nombreSP, String nombreBD, Exception objexception) {
		super(codError, msjError, nombreSP, nombreBD, objexception);
		// TODO Auto-generated constructor stub
	}

}
