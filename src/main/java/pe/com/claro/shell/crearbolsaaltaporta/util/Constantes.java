package pe.com.claro.shell.crearbolsaaltaporta.util;

public class Constantes {

	public final static String COMA = ",";
	public final static char PUNTO = '.';
	public final static char GUION_BAJO = '_';
	public final static boolean TRUE = true;
	public final static boolean FALSE = false;

	public final static String LOG4J_PROPERTIES = "log4j.properties";
	public final static String CONFIG_PATH = "applicationContext.xml";
	
	public final static String BD_WS = "[BD_WS]";
	public final static String RECURSO = "[RECURSO]";
	public final static String VACIO = "";
	public final static String ESPACIO = " ";
	public final static String SALTOLINEA = "\n";
	
	public final static String MILISEGUNDOS = "milisegundos";

}
