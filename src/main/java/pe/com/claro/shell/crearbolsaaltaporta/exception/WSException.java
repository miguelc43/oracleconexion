package pe.com.claro.shell.crearbolsaaltaporta.exception;

public class WSException extends BaseException {

	private static final long serialVersionUID = 7070437867800228547L;

	public WSException() {
		super();
		// TODO Auto-generated constructor stub
	}

	public WSException(Exception objexception) {
		super(objexception);
		// TODO Auto-generated constructor stub
	}

	public WSException(String msjError) {
		super(msjError);
		// TODO Auto-generated constructor stub
	}

	public WSException(String codError, String msjError, Exception objexception) {
		super(codError, msjError, objexception);
		// TODO Auto-generated constructor stub
	}

	public WSException(String codError, String msjError, String nombreWS, Exception objexception) {
		super(codError, msjError, nombreWS, objexception);
		// TODO Auto-generated constructor stub
	}

}
