package pe.com.claro.shell.crearbolsaaltaporta.exception;

public class BaseException extends Exception {

	private static final long serialVersionUID = 2141005611296085858L;

	private Exception objexception;
	private String codError;
	private String msjError;
	private String nombreSP;
	private String nombreBD;
	private String nombreWS;
	private String nombreQueue;

	public BaseException() {
		super();
	}

	public BaseException(Exception objexception) {
		this.objexception = objexception;
	}

	public BaseException(String msjError) {
		super(msjError);
		this.msjError = msjError;
	}

	public BaseException(String msjError, Exception objexception) {
		super(objexception);
		this.objexception = objexception;
		this.msjError = msjError;
	}

	public BaseException(String codError, String msjError, Exception objexception) {
		super(msjError);
		this.codError = codError;
		this.msjError = msjError;
		this.objexception = objexception;
	}

	public BaseException(String codError, String msjError, String nombreSP, String nombreBD, Exception objexception) {
		super(msjError);
		this.codError = codError;
		this.msjError = msjError;
		this.nombreSP = nombreSP;
		this.nombreBD = nombreBD;
		this.objexception = objexception;
	}

	public BaseException(String codError, String msjError, String nombreWS, Exception objexception) {
		super(msjError);
		this.codError = codError;
		this.msjError = msjError;
		this.nombreWS = nombreWS;
		this.objexception = objexception;
	}

	/**
	 * @return the objexception
	 */
	public Exception getObjexception() {
		return objexception;
	}

	/**
	 * @param objexception
	 *            the objexception to set
	 */
	public void setObjexception(Exception objexception) {
		this.objexception = objexception;
	}

	/**
	 * @return the codError
	 */
	public String getCodError() {
		return codError;
	}

	/**
	 * @param codError
	 *            the codError to set
	 */
	public void setCodError(String codError) {
		this.codError = codError;
	}

	/**
	 * @return the msjError
	 */
	public String getMsjError() {
		return msjError;
	}

	/**
	 * @param msjError
	 *            the msjError to set
	 */
	public void setMsjError(String msjError) {
		this.msjError = msjError;
	}

	/**
	 * @return the nombreSP
	 */
	public String getNombreSP() {
		return nombreSP;
	}

	/**
	 * @param nombreSP
	 *            the nombreSP to set
	 */
	public void setNombreSP(String nombreSP) {
		this.nombreSP = nombreSP;
	}

	/**
	 * @return the nombreBD
	 */
	public String getNombreBD() {
		return nombreBD;
	}

	/**
	 * @param nombreBD
	 *            the nombreBD to set
	 */
	public void setNombreBD(String nombreBD) {
		this.nombreBD = nombreBD;
	}

	/**
	 * @return the nombreWS
	 */
	public String getNombreWS() {
		return nombreWS;
	}

	/**
	 * @param nombreWS
	 *            the nombreWS to set
	 */
	public void setNombreWS(String nombreWS) {
		this.nombreWS = nombreWS;
	}

	/**
	 * @return the nombreQueue
	 */
	public String getNombreQueue() {
		return nombreQueue;
	}

	/**
	 * @param nombreQueue
	 *            the nombreQueue to set
	 */
	public void setNombreQueue(String nombreQueue) {
		this.nombreQueue = nombreQueue;
	}
}
